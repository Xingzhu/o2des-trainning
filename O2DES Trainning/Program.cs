﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using O2DESNet;



namespace O2DES_Trainning
{
    class Program
    {
        static void Main(string[] args)
        {
            // Step I – Configuration (instantialize static properties)
            MM1Queue.Statics config = new MM1Queue.Statics
            {
                NServers = 3,
                HourlyArrivalRate = 30,
                HourlyServiceRate = 12
            };

            // Step II / III – Construct model -> Initialize simulator
            Simulator sim = new Simulator(new MM1Queue(config, 0));
            // Set the log output (via Log method)
            sim.WarmUp(TimeSpan.FromHours(3));
            sim.State.Display = true;
            sim.State.LogFile = "log.txt";
            // Run in loop
            while (true)
            {
                sim.Run(1); // Run one event at a time
                sim.WriteToConsole(); // Display state to console
                Console.ReadKey();
            }

        }
    }
}
